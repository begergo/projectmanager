﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbHandlerList
{
    public interface IContainer
    {
        int Id { get; set; }
        void Save();
        void Update();
        void Delete();
    }
}

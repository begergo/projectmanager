﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbHandlerList
{
    public class DbHandler
    {
        //Singleton
        private static IDbOp operation;
        private static DbHandler handler;

        private DbHandler() { } 
        public static DbHandler GetInstance(IDbOp operation)
        {
            if (handler==null)
            {
                handler = new DbHandler();
            }
            DbHandler.operation = operation; 
            return handler;
        }

        public void Insert(IContainer e)
        {
            operation.Insert(e);
        }

        public void Update(IContainer e)
        {
            operation.Update(e);
        }

        public void Delete(int id)
        {
            operation.Delete(id);
        }

        public IList<IContainer> SelectAll()
        {
            return operation.SelectAll();
        }
    }
}

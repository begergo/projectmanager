﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbHandlerList
{
    public interface IDbOp
    {
        void Insert(IContainer e);
        void Update(IContainer e);
        void Delete(int id);
        IList<IContainer> SelectAll();
    }
}

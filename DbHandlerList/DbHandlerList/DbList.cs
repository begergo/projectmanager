﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DbHandlerList
{
    //dll-be teszem ki
    public class DbList<T> : IList<T> where T : IContainer{
        private List<T> list;
      
        public DbList(){
            list = new List<T>();
        }

        public T this[int index]{
            get
            {
                return list[index];
            }

            set
            {
                list[index] = value;
            }
        }

        public int Count{
            get
            {
                return list.Count;
            }
        }

        public bool IsReadOnly{
            get
            {
                return false; 
            }
        }

        public void Add(T item) {
            item.Save();
            list.Add(item);
        }

        public void Clear(){
            foreach (T item in list)
            {
                item.Delete();                
            }
            list.Clear();
        }

        public bool Contains(T item)
        {
            return list.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            list.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return list.GetEnumerator();
        }

        public int IndexOf(T item)
        {
            return list.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            item.Save();
            list.Insert(index, item);
        }

        public bool Remove(T item)
        {
            item.Delete();
            return list.Remove(item);
        }

        public void RemoveAt(int index)
        {
            T item = list[index];
            item.Delete();
            list.RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return list.GetEnumerator();
        }
    }
}

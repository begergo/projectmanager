﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbHandlerList;

namespace projectManager
{
  class User : IUser
  {

    public int id { get; set; }
    public string name { get; set; }
    public string password { get; set; }
    public string realName { get; set; }
    public string email { get; set; }
    public int projectID { get; set; }
    public int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

    public int CompareTo(IUser other){
      return ((User)other).name.CompareTo(name);
    }

    public void Delete() {
      if (id > 0) {
        DbHandler.GetInstance(new UserHandler()).Delete(id);
      }
    }

    public void Save(){
      if (id == 0){
        DbHandler.GetInstance(new UserHandler()).Insert(this);
      }
    }
    
    public void Update() {
      throw new NotImplementedException();
    }

    public static DbList<User> GetAll(){
      IList<IContainer> list = DbHandler.GetInstance(new UserHandler()).SelectAll();
      DbList<User> result = new DbList<User>();
      foreach (IUser item in list)
      {
        result.Add((User)item);
      }
      return result;
    }
    
  }
}

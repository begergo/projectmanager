﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbHandlerList;
using System.Data.SqlClient;

namespace projectManager
{
  class UserHandler : ConnectionHandler, IDbOp
  {
    public void Delete(int id)
    {

      conn.Open();
      SqlCommand cmd = new SqlCommand("delete from [dbo].[user] where [ID]=" + id, conn);
      cmd.ExecuteNonQuery();

      conn.Close();
    }

    public void Insert(IContainer e)
    {
      User u = (User)e;
      conn.Open();

      SqlCommand cmd = new SqlCommand(
         @"INSERT INTO [dbo].[user]
           ([name]
           ,[password]
           ,[realName]
           ,[email]
           ,[projectID]
          )
     VALUES
           ('" + u.name + "','" + u.password + "','" + u.realName + "','" + u.email + "','" + u.projectID + "')", conn);
      cmd.ExecuteNonQuery();

      cmd.CommandText = "select max(ID) from [dbo].[user]";
      SqlDataReader reader = cmd.ExecuteReader();
      reader.Read();
      int id = reader.GetInt32(0);
      reader.Close();
      u.Id = id;
      conn.Close();
    }

    public IList<IContainer> SelectAll()
    {
      conn.Open();
      SqlCommand command = new SqlCommand("select * from [dbo].[user]", conn);
      command.ExecuteNonQuery();
      SqlDataReader reader = command.ExecuteReader();
      IList<IContainer> list = new DbList<IContainer>();
      while (reader.Read())
      {
        User temp = new User();
        temp.id = reader.GetInt32(0);
        temp.name = reader.GetString(1);
        temp.password = reader.GetString(2);
        temp.realName = reader.GetString(3);
        temp.email = reader.GetString(4);
        temp.projectID = reader.GetInt32(5);
        list.Add(temp);
      }
      reader.Close();
      conn.Close();
      return list;
    }

    public void Update(IContainer e)
    {
      throw new NotImplementedException();
    }
  }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projectManager
{
  public partial class DashBoard : Form
  {
    BindingList<Task> tasks;
    public DashBoard(bool isAdmin)
    {
      InitializeComponent();
      tasks = new BindingList<Task>(Task.GetAll());
      String des;
      DateTime d = getMinTargetDay(out des);
      richTextBox1.Text = des;
      label3.Text = d.ToString();
      label5.Text = ((int)(d-DateTime.Now).TotalDays).ToString();
    }

    private void label1_Click(object sender, EventArgs e)
    {

    }

    private void label5_Click(object sender, EventArgs e)
    {

    }

    private void button1_Click(object sender, EventArgs e)
    {
      TaskForm tf = new TaskForm();
      tf.MdiParent = this.MdiParent;
      MainForm.FormatingForms(tf);
      tf.Show();
      this.Dispose();
    }

    public DateTime getMinTargetDay(out String des) {
      des = "";
      DateTime max = DateTime.MinValue;
      for (int i = 0;  i < tasks.Count; i++) {
        if (DateTime.Compare(tasks.ElementAt(i).targetDate, max) == 1)
        {
          max = tasks.ElementAt(i).targetDate;
          des = tasks.ElementAt(i).description;
        }
      }
      return max;
    }

    private void label3_Click(object sender, EventArgs e)
    {

    }
  }
}

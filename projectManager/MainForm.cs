﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projectManager
{
  public partial class MainForm : Form
  {
    private int childFormNumber = 0;

    public MainForm()
    {
      InitializeComponent();
      DashBoard db = new DashBoard(true);
      db.MdiParent = this;
      FormatingForms(db);
      db.Show();
    }
    
    private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    public static void FormatingForms(Form f) {
      f.Dock = DockStyle.Fill;
      f.FormBorderStyle = FormBorderStyle.None;
      f.ControlBox = false;
      f.Text = String.Empty;
    }
    

  }
}

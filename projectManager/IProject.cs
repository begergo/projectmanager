﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbHandlerList;

namespace projectManager
{
  public interface IProject: IContainer, IComparable<IProject>
  {
  }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbHandlerList;
using System.Data.SqlClient;

namespace projectManager
{
  class ProjectHandler : ConnectionHandler, IDbOp
  {
    public void Delete(int id)
    {

      conn.Open();
      SqlCommand cmd = new SqlCommand("delete from [dbo].[project] where [ID]=" + id, conn);
      cmd.ExecuteNonQuery();

      conn.Close();
    }

    public void Insert(IContainer e)
    {
      Project p = (Project)e;
      conn.Open();

      SqlCommand cmd = new SqlCommand(
         @"INSERT INTO [dbo].[project]
           ([name]
           ,[userID]
          )
     VALUES
           ('" + p.name + "','" + p.userID + "')", conn);
      cmd.ExecuteNonQuery();
      cmd.CommandText = "select max(ID) from [dbo].[project]";
      SqlDataReader reader = cmd.ExecuteReader();
      reader.Read();
      int id = reader.GetInt32(0);
      reader.Close();
      p.Id = id;
      conn.Close();
    }

    public IList<IContainer> SelectAll()
    {
      conn.Open();
      SqlCommand command = new SqlCommand("select * from [dbo].[project]", conn);
      command.ExecuteNonQuery();
      SqlDataReader reader = command.ExecuteReader();
      IList<IContainer> list = new DbList<IContainer>();
      while (reader.Read())
      {
        Project temp = new Project();
        temp.id = reader.GetInt32(0);
        temp.name = reader.GetString(1);
        temp.userID = reader.GetInt32(2);
        list.Add(temp);
      }
      reader.Close();
      conn.Close();
      return list;
    }

    public void Update(IContainer e)
    {
      throw new NotImplementedException();
    }
  }
}

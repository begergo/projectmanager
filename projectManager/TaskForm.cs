﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projectManager
{
  public partial class TaskForm : Form
  {
    int deleteSelectorTask = 0;
    BindingList<Task> tasks;
    TaskHandler th;
    public TaskForm()
    {
      th = new TaskHandler();
      InitializeComponent();
      LoadData();
      if (tasks.ElementAt(0).id != null)
      {
        deleteSelectorTask = tasks.ElementAt(0).id;
      }
    }

    public void LoadData() {

      Console.WriteLine("LoadData");
      tasks = new BindingList<Task>(Task.GetAll());
      
      dataGridView1.Rows.Clear();
      for (int i = 0; i < tasks.Count; i++){

        int n = dataGridView1.Rows.Add();
        if (tasks.ElementAt(i).type == Type.TO_DO) {
          dataGridView1.Rows[n].Cells[1].Value = tasks.ElementAt(i).description.ToString();
          dataGridView1.Rows[n].Cells[0].Value = tasks.ElementAt(i).id.ToString();
        }
        if (tasks.ElementAt(i).type == Type.IN_PROGRESS)
        {
          dataGridView1.Rows[n].Cells[2].Value = tasks.ElementAt(i).description.ToString();
          dataGridView1.Rows[n].Cells[0].Value = tasks.ElementAt(i).id.ToString();
        }
        if (tasks.ElementAt(i).type == Type.DONE)
        {
          dataGridView1.Rows[n].Cells[3].Value = tasks.ElementAt(i).description.ToString();
          dataGridView1.Rows[n].Cells[0].Value = tasks.ElementAt(i).id.ToString();

        }
      }
    }

    private void button1_Click(object sender, EventArgs e)
    {
      NewTaskForm ntf = new NewTaskForm(this);
      ntf.Show();
    }

   

    private void button2_Click(object sender, EventArgs e)
    {
      DialogResult dialogResult = MessageBox.Show("Are you sure?", "Delete", MessageBoxButtons.YesNo);
      if (dialogResult == DialogResult.Yes)
      {
        th.Delete(Convert.ToInt32(deleteSelectorTask));
        LoadData();
      }
      else if (dialogResult == DialogResult.No)
      {
        //do something else
      }

    }

    private void dataGridView1_MouseClick(object sender, MouseEventArgs e)
    {

      deleteSelectorTask = Convert.ToInt32(dataGridView1.SelectedRows[0].Cells[0].Value.ToString());
      Console.WriteLine("Selected");
    }



    private void button3_Click(object sender, EventArgs e)
    {
      for (int i = 0; i < tasks.Count; i++)
      {

        Console.WriteLine("kaki");

        Console.WriteLine(tasks.ElementAt(i).id);
        Console.WriteLine(deleteSelectorTask);

        if (tasks.ElementAt(i).id == deleteSelectorTask)
        {

          if (tasks.ElementAt(i).type == Type.IN_PROGRESS)
          {
            th.Update(deleteSelectorTask, Type.TO_DO.ToString());
          }
          if (tasks.ElementAt(i).type == Type.DONE)
          {
            th.Update(deleteSelectorTask, Type.IN_PROGRESS.ToString());
          }
        }
      }
      LoadData();
    }

    private void button4_Click(object sender, EventArgs e)
    {
      for (int i = 0; i < tasks.Count; i++)
      {
        if (tasks.ElementAt(i).id == deleteSelectorTask)
        {
          if (tasks.ElementAt(i).type == Type.TO_DO)
          {
            th.Update(deleteSelectorTask, Type.IN_PROGRESS.ToString());
          }
          if (tasks.ElementAt(i).type == Type.IN_PROGRESS)
          {
            th.Update(deleteSelectorTask, Type.DONE.ToString());
          }
        }
      }
      LoadData();
    }

    private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {

    }
  }
}

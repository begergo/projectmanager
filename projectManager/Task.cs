﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbHandlerList;

namespace projectManager
{

  public enum Type { TO_DO, IN_PROGRESS, DONE };

  class Task : ITask
  {

    public int id { get; set; }
    public int projectID { get; set; }
    public Type type { get; set; }
    public int rank { get; set; }
    public DateTime targetDate { get; set; }
    public string description { get; set; }
    public int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

    public int CompareTo(ITask other)
    {
      return ((Task)other).id.CompareTo(id);
    }

    public void Delete()
    {
      if (id > 0)
      {
        DbHandler.GetInstance(new TaskHandler()).Delete(id);
      }
    }

    public void Save()
    {
      if (id == 0)
      {
        DbHandler.GetInstance(new TaskHandler()).Insert(this);
      }
    }

    public void Update()
    {
      throw new NotImplementedException();
    }

    public static DbList<Task> GetAll()
    {
      IList<IContainer> list = DbHandler.GetInstance(new TaskHandler()).SelectAll();
      DbList<Task> result = new DbList<Task>();
      foreach (ITask item in list)
      {
        result.Add((Task)item);
      }
      return result;
    }

  }
}

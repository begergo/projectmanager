﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbHandlerList;

namespace projectManager
{
  class Project : IProject
  {

    public int id { get; set; }
    public string name { get; set; }
    public int userID { get; set; }
    public int Id { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

    public int CompareTo(IProject other)
    {
      return ((Project)other).name.CompareTo(name);
    }

    public void Delete()
    {
      if (id > 0)
      {
        DbHandler.GetInstance(new ProjectHandler()).Delete(id);
      }
    }

    public void Save()
    {
      if (id == 0)
      {
        DbHandler.GetInstance(new ProjectHandler()).Insert(this);
      }
    }

    public void Update()
    {
      throw new NotImplementedException();
    }

    public static DbList<Project> GetAll()
    {
      IList<IContainer> list = DbHandler.GetInstance(new ProjectHandler()).SelectAll();
      DbList<Project> result = new DbList<Project>();
      foreach (IProject item in list)
      {
        result.Add((Project)item);
      }
      return result;
    }

  }
}

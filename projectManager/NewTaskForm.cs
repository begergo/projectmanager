﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace projectManager
{
  public partial class NewTaskForm : Form
  {
    TaskForm tf;
    public NewTaskForm(TaskForm form)
    {
      tf = form;

      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NewTaskForm_FormClosing);
      InitializeComponent();
      comboBox1.Text = "0";
      for (int i = 0; i < 5; i++) {
        comboBox1.Items.Insert(i, (i+1).ToString());
      }

      comboBox2.Text = "-SELECT-";
      comboBox2.Items.Insert(0, Type.DONE);
      comboBox2.Items.Insert(0, Type.IN_PROGRESS);
      comboBox2.Items.Insert(0, Type.TO_DO);
    }
    
    private void button1_Click(object sender, EventArgs e)
    {
      TaskHandler th = new TaskHandler();
      Task t = new Task();
      t.projectID = 0;
      t.type = (Type)Enum.Parse(typeof(Type), comboBox2.Text);
      t.rank = Convert.ToInt32(comboBox1.Text);
      t.targetDate = dateTimePicker1.Value.Date;
      t.description = richTextBox1.Text;
      th.Insert(t);
      this.Close();
    }

    private void NewTaskForm_FormClosing(object sender, FormClosingEventArgs e)
    {
      Console.WriteLine("hello");
      tf.LoadData();
    }
  }
}

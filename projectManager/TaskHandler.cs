﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbHandlerList;
using System.Data.SqlClient;

namespace projectManager
{
  class TaskHandler : ConnectionHandler, IDbOp
  {
    public void Delete(int id)
    {

      conn.Open();
      SqlCommand cmd = new SqlCommand("delete from [dbo].[task] where [ID]=" + id, conn);
      cmd.ExecuteNonQuery();

      conn.Close();
    }

    public void Insert(IContainer x)
    {
      Task t = (Task)x;
      conn.Open();

      SqlCommand cmd = new SqlCommand(
         @"INSERT INTO [dbo].[task]
           ([projectID]
           ,[type]
           ,[rank]
           ,[targetDate]
           ,[description]
          )
     VALUES
           ('" + t.projectID + "','" + t.type + "','" + t.rank + "','" + t.targetDate + "','" + t.description + "')", conn);
      cmd.ExecuteNonQuery();

      cmd.CommandText = "select max(ID) from [dbo].[task]";
      SqlDataReader reader = cmd.ExecuteReader();
      reader.Read();
      int id = reader.GetInt32(0);
      reader.Close();
      t.id = id;
      conn.Close();
    }

    public IList<IContainer> SelectAll()
    {
      conn.Open();
      SqlCommand command = new SqlCommand("select * from [dbo].[task]", conn);
      command.ExecuteNonQuery();
      SqlDataReader reader = command.ExecuteReader();
      IList<IContainer> list = new DbList<IContainer>();
      while (reader.Read())
      {
        Task temp = new Task();
        temp.id = reader.GetInt32(0);
        temp.projectID = reader.GetInt32(1);
        temp.type = (Type)Enum.Parse(typeof(Type), reader.GetString(2));
        temp.rank = reader.GetInt32(3);
        temp.targetDate = reader.GetDateTime(4);
        temp.description = reader.GetString(5);
        list.Add(temp);
      }
      reader.Close();
      conn.Close();
      return list;
    }

    public void Update(IContainer e)
    {
      Task t = (Task)e;
      conn.Open();
      SqlCommand cmd = new SqlCommand("UDATE [dbo].[task] SET[type] = '" + t.type.ToString() + "' WHERE[ID] = '" + t.id.ToString() + "'",conn);
      cmd.ExecuteNonQuery();
      conn.Close();
    }

    public void Update(int id,String type)
    {
      conn.Open();
      SqlCommand cmd = new SqlCommand("UPDATE [dbo].[task] SET[type] = '" + type + "' WHERE[ID] = '" + id.ToString() + "'", conn);
      cmd.ExecuteNonQuery();
      conn.Close();
    }
  }
}

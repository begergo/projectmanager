/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2014 (12.0.2269)
    Source Database Engine Edition : Microsoft SQL Server Express Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2017
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/
USE [master]
GO
/****** Object:  Database [projectMan]    Script Date: 11/8/2017 1:52:53 PM ******/
CREATE DATABASE [projectMan]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'projectMan', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\projectMan.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'projectMan_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\projectMan_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [projectMan] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [projectMan].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [projectMan] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [projectMan] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [projectMan] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [projectMan] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [projectMan] SET ARITHABORT OFF 
GO
ALTER DATABASE [projectMan] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [projectMan] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [projectMan] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [projectMan] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [projectMan] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [projectMan] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [projectMan] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [projectMan] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [projectMan] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [projectMan] SET  DISABLE_BROKER 
GO
ALTER DATABASE [projectMan] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [projectMan] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [projectMan] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [projectMan] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [projectMan] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [projectMan] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [projectMan] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [projectMan] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [projectMan] SET  MULTI_USER 
GO
ALTER DATABASE [projectMan] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [projectMan] SET DB_CHAINING OFF 
GO
ALTER DATABASE [projectMan] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [projectMan] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [projectMan] SET DELAYED_DURABILITY = DISABLED 
GO
USE [projectMan]
GO
/****** Object:  Table [dbo].[project]    Script Date: 11/8/2017 1:52:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[project](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[userID] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[task]    Script Date: 11/8/2017 1:52:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[task](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[projectID] [int] NOT NULL,
	[type] [varchar](50) NOT NULL,
	[rank] [int] NULL,
	[targetDate] [date] NOT NULL,
	[description] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user]    Script Date: 11/8/2017 1:52:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[realName] [varchar](50) NOT NULL,
	[email] [varchar](50) NULL,
	[projectID] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[user] ADD  CONSTRAINT [DF_user_projectID]  DEFAULT ((0)) FOR [projectID]
GO
USE [master]
GO
ALTER DATABASE [projectMan] SET  READ_WRITE 
GO
